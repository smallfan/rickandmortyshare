package com.rm.util

const val BASE_URL = "https://rickandmortyapi.com/api/"
const val EPISODES_OF_FIRST_SEASON = "1,2,3,4,5,6,7,8,9,10,11"
const val EPISODES_OF_SECOND_SEASON = "12,13,14,15,16,17,18,19,20,21"
const val EPISODES_OF_THIRD_SEASON = "22,23,24,25,26,27,28,29,30,31"
const val EPISODES_OF_FOURTH_SEASON = "32,33,34,35,36,37,38,39,40,41"
const val EPISODES_OF_FIFTH_SEASON = "42,43,44,45,46,47,48,49,50,51"