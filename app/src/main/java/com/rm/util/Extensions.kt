package com.rm.util

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.rm.R

fun ImageView.setImageByUrl(imgUrl: String) {
    if (imgUrl.isNotEmpty()) {
        Glide.with(context)
            .load(imgUrl)
            .apply(RequestOptions()
                .placeholder(R.drawable.loading_animation)
                .error(R.drawable.ic_rm_error))
            .into(this)
    } else {
        this.setImageResource(R.drawable.ic_rm_default)
    }
}

fun Int?.orZero() = this ?: 0