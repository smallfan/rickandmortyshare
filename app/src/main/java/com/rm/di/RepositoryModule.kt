package com.rm.di

import com.rm.data.db.data_sources.EpisodesDbDataSource
import com.rm.data.remote.data_sources.CharactersDataSource
import com.rm.data.remote.data_sources.EpisodesDataSource
import com.rm.domain.AppRepository
import com.rm.domain.AppRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideRepository(
        episodesDataSource: EpisodesDataSource,
        charactersDataSource: CharactersDataSource,
        episodesDbDataSource: EpisodesDbDataSource
    ): AppRepository {
        return AppRepositoryImpl(episodesDataSource, charactersDataSource, episodesDbDataSource)
    }
}