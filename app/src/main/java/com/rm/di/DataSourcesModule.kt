package com.rm.di

import com.rm.data.db.AppDatabase
import com.rm.data.db.data_sources.EpisodesDbDataSource
import com.rm.data.db.data_sources.EpisodesDbDataSourceImpl
import com.rm.data.db.mappers.EpisodeMapperEnt
import com.rm.data.db.mappers.EpisodeMapperEntImpl
import com.rm.data.remote.api.ApiService
import com.rm.data.remote.data_sources.CharactersDataSource
import com.rm.data.remote.data_sources.CharactersDataSourceImpl
import com.rm.data.remote.data_sources.EpisodesDataSource
import com.rm.data.remote.data_sources.EpisodesDataSourceImpl
import com.rm.data.remote.mappers.CharacterMapperDto
import com.rm.data.remote.mappers.CharacterMapperDtoImpl
import com.rm.data.remote.mappers.EpisodeMapperDto
import com.rm.data.remote.mappers.EpisodeMapperImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataSourcesModule {

    @Provides
    @Singleton
    fun provideEpisodesDtoMapper(): EpisodeMapperDto {
        return EpisodeMapperImpl()
    }

    @Provides
    @Singleton
    fun provideCharactersDtoMapper(): CharacterMapperDto {
        return CharacterMapperDtoImpl()
    }

    @Provides
    @Singleton
    fun provideEpisodesMapperEnt(): EpisodeMapperEnt {
        return EpisodeMapperEntImpl()
    }

    @Provides
    @Singleton
    fun provideEpisodesDataSource(service: ApiService, appDatabase: AppDatabase, mapper: EpisodeMapperDto): EpisodesDataSource {
        return EpisodesDataSourceImpl(service, appDatabase,  mapper)
    }

    @Provides
    @Singleton
    fun provideEpisodesDbDataSource(appDatabase: AppDatabase, mapper: EpisodeMapperEnt): EpisodesDbDataSource {
        return EpisodesDbDataSourceImpl(appDatabase,  mapper)
    }

    @Provides
    @Singleton
    fun provideCharactersDataSource(service: ApiService, mapper: CharacterMapperDto): CharactersDataSource {
        return CharactersDataSourceImpl(service, mapper)
    }
}