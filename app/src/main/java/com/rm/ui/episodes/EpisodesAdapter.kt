package com.rm.ui.episodes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.rm.databinding.EpisodesItemBinding
import com.rm.domain.models.Episode

class EpisodesAdapter(
    var items: MutableList<Episode> = mutableListOf(),
) : RecyclerView.Adapter<EpisodeViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): EpisodeViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return EpisodeViewHolder(EpisodesItemBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: EpisodeViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun updateList(newItems: List<Episode>) {
        val fullListItems = mutableListOf<Episode>()
        fullListItems.addAll(items)
        fullListItems.addAll(newItems)

        val diffCallback = EpisodesDiffCallback(items, fullListItems)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        diffResult.dispatchUpdatesTo(this)
        items.addAll(newItems)
    }
}

class EpisodeViewHolder(
    private val binding: EpisodesItemBinding,
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(item: Episode) {
        binding.nameTxt.text = item.name
        binding.airDateTxt.text = item.air_date
        binding.charactersTxt.visibility = View.GONE
        binding.urlTxt.visibility = View.GONE
        binding.episodeTxt.text = item.episode
        binding.createdTxt.visibility = View.GONE
    }
}