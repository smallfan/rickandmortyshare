package com.rm.ui.episodes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.rm.R
import com.rm.databinding.EpisodesFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class EpisodesFragment : Fragment() {

    private var _binding: EpisodesFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: EpisodesViewModel by viewModels()

    private lateinit var episodesAdapter: EpisodesAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        super.onCreate(savedInstanceState)
        _binding = EpisodesFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()
        collect()

        val episodesNumber = requireArguments().getString(EPISODES_NUMBER).orEmpty()
        viewModel.getEpisodesOfSeason(episodesNumber)
    }

    private fun collect() {
        lifecycleScope.launchWhenStarted {
            launch {
                viewModel.episodesState.collect { episodeList ->
                    episodesAdapter.updateList(episodeList)
                }
            }
            launch {
                viewModel.loadingState.collect { isVisible ->
                    binding.progressBar.isVisible = isVisible
                }
            }
            launch {
                viewModel.connectionState.collect { connectionState ->
                    if(!connectionState) {
                        val toast = Toast.makeText(requireContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT)
                        toast.show()
                    }
                }
            }
        }
    }

    private fun initAdapter() {
        episodesAdapter = EpisodesAdapter()
        binding.episodesList.adapter = episodesAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.episodesList.adapter = null
        _binding = null
    }

    companion object {
        const val EPISODES_NUMBER = "episodesNumber"
        fun getBundle(episodeNumbers: String): Bundle {
            return bundleOf(EPISODES_NUMBER to episodeNumbers)
        }
    }
}