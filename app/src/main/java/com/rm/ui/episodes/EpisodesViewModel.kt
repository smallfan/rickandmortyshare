package com.rm.ui.episodes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rm.data.remote.NetworkResult
import com.rm.domain.AppRepository
import com.rm.domain.models.Episode
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class EpisodesViewModel @Inject constructor(
    private val appRepository: AppRepository,
) : ViewModel() {

    private val _episodesState: MutableSharedFlow<List<Episode>> = MutableSharedFlow()
    val episodesState: SharedFlow<List<Episode>> = _episodesState.asSharedFlow()

    private val _loadingState: MutableSharedFlow<Boolean> = MutableSharedFlow()
    val loadingState: SharedFlow<Boolean> = _loadingState.asSharedFlow()

    private val _connectionState: MutableSharedFlow<Boolean> = MutableSharedFlow()
    val connectionState: SharedFlow<Boolean> = _connectionState.asSharedFlow()

    fun getEpisodesOfSeason(episodes: String) {
        appRepository.getEpisodesOfSeason(episodes)
            .flowOn(Dispatchers.IO)
            .onStart {
                _loadingState.emit(true)
            }
            .onCompletion {
                _loadingState.emit(false)
            }
            .onEach {
                when(it) {
                    is NetworkResult.Success -> {
                        val data = (it as? NetworkResult.Success<List<Episode>, Throwable>)?.data.orEmpty()
                        _episodesState.emit(data)
                    }
                    is NetworkResult.Exception -> {
                        println(this)
                    }
                    is NetworkResult.NoInternet -> {
                        _connectionState.emit(false)
                    }
                    is NetworkResult.Timeout -> {}
                }
            }
            .launchIn(viewModelScope)
    }
}