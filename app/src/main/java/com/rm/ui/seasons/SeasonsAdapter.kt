package com.rm.ui.seasons

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rm.databinding.SeasonsItemBinding
import com.rm.domain.models.Season

class SeasonsAdapter(
    var items: MutableList<Season> = mutableListOf(),
    private var onItemClicked: ((episodesOfSeason: String) -> Unit)
) : RecyclerView.Adapter<SeasonViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): SeasonViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return SeasonViewHolder(SeasonsItemBinding.inflate(layoutInflater, parent, false), onItemClicked)
    }

    override fun onBindViewHolder(holder: SeasonViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class SeasonViewHolder(
    private val binding: SeasonsItemBinding,
    private var onItemClicked: ((episodesOfSeason: String) -> Unit)
) : RecyclerView.ViewHolder(binding.root) {
    private val context: Context = binding.root.context
    fun bind(item: Season) {
        binding.seasonTxt.text = context.getString(item.seasonNumber)
        binding.seasonImg.setImageResource(item.seasonPoster)
        binding.root.setOnClickListener {
            onItemClicked(item.episodesOfSeason)
        }
    }
}