package com.rm.ui.seasons

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.rm.R
import com.rm.databinding.SeasonsFragmentBinding
import com.rm.domain.models.Season
import com.rm.ui.episodes.EpisodesFragment
import com.rm.util.*

class SeasonsFragment : Fragment() {

    private var _binding: SeasonsFragmentBinding? = null
    private val binding get() = _binding!!

    private lateinit var seasonsAdapter: SeasonsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        super.onCreate(savedInstanceState)
        _binding = SeasonsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()

        val seasons = listOf(
            Season(
                R.string.first_season,
                R.drawable.season1,
                EPISODES_OF_FIRST_SEASON),
            Season(
                R.string.second_season,
                R.drawable.season2,
                EPISODES_OF_SECOND_SEASON),
            Season(
                R.string.third_season,
                R.drawable.season3,
                EPISODES_OF_THIRD_SEASON),
            Season(
                R.string.fourth_season,
                R.drawable.season4,
                EPISODES_OF_FOURTH_SEASON),
            Season(
                R.string.fifth_season,
                R.drawable.season5,
                EPISODES_OF_FIFTH_SEASON),
        )
        seasonsAdapter.items.addAll(seasons)
    }

    private fun initAdapter() {
        seasonsAdapter = SeasonsAdapter { episodesOfSeason ->
            val bundle = EpisodesFragment.getBundle(episodesOfSeason)
            findNavController().navigate(R.id.action_seasonsFragment_to_episodesFragment, bundle)
        }
        binding.seasonsList.adapter = seasonsAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.seasonsList.adapter = null
        _binding = null
    }
}