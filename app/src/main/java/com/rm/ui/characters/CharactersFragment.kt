package com.rm.ui.characters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rm.R
import com.rm.databinding.CharactersFragmentBinding
import com.rm.util.orZero
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class CharactersFragment : Fragment() {

    private var _binding: CharactersFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: CharactersViewModel by viewModels()

    private lateinit var charactersAdapter: CharactersAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        super.onCreate(savedInstanceState)
        _binding = CharactersFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()
        collect()

        binding.charactersList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager = LinearLayoutManager::class.java.cast(recyclerView.layoutManager)
                val totalItemCount = layoutManager?.itemCount.orZero()
                val lastVisible = layoutManager?.findLastVisibleItemPosition().orZero()
                val endHasBeenReached = lastVisible + 5 >= totalItemCount
                if (totalItemCount > 0 && endHasBeenReached) {
                    viewModel.getCharacterNextPage()
                }
            }
        })
        viewModel.getCharacterFirstPage()
    }

    private fun collect() {
        lifecycleScope.launchWhenStarted {
            launch {
                viewModel.charactersState.collect { characterList ->
                    if(characterList.isNotEmpty()) binding.progressBar.visibility = ProgressBar.GONE
                    charactersAdapter.updateList(characterList)
                }
            }
            launch {
                viewModel.loadingState.collect { isVisible ->
                    binding.progressBar.isVisible = isVisible
                }
            }
            launch {
                viewModel.connectionState.collect { connectionState ->
                    if(!connectionState) {
                        val toast = Toast.makeText(requireContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT)
                        toast.show()
                    }
                }
            }
        }
    }

    private fun initAdapter() {
        charactersAdapter = CharactersAdapter()
        binding.charactersList.adapter = charactersAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.charactersList.adapter = null
        _binding = null
    }
}