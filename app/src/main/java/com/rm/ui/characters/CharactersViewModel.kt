package com.rm.ui.characters

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rm.data.remote.NetworkResult
import com.rm.domain.AppRepository
import com.rm.domain.models.Character
import com.rm.domain.models.CharacterResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class CharactersViewModel @Inject constructor(
    private val appRepository: AppRepository,
) : ViewModel() {

    private val _charactersState: MutableStateFlow<List<Character>> = MutableStateFlow(emptyList())
    val charactersState: StateFlow<List<Character>> = _charactersState.asStateFlow()

    private val _loadingState: MutableSharedFlow<Boolean> = MutableSharedFlow()
    val loadingState: SharedFlow<Boolean> = _loadingState.asSharedFlow()

    private val _connectionState: MutableSharedFlow<Boolean> = MutableSharedFlow()
    val connectionState: SharedFlow<Boolean> = _connectionState.asSharedFlow()

    var page = 0
    var flag = true

    fun getCharacterFirstPage() {
        page = 0
        getCharacters(page)
    }

    fun getCharacterNextPage() {
        if(flag) {
            flag = false
            page++
            getCharacters(page)
        }
    }

    private fun getCharacters(page: Int) {
            Log.i("Pages", page.toString())
            appRepository.getCharacters(page)
                .flowOn(Dispatchers.IO)
                .onStart {
                    _loadingState.emit(true)
                }
                .onCompletion {
                    _loadingState.emit(false)
                }
                .onEach {
                    when(it) {
                        is NetworkResult.Success -> {
                            val data = (it as? NetworkResult.Success<CharacterResponse, Throwable>)?.data
                            data?.results?.let { charList ->
                                val newArray = charactersState.value.toMutableList().apply {
                                    addAll(charList)
                                }
                                _charactersState.emit(newArray)
                            }
                        }
                        is NetworkResult.Exception -> {
                            println(this)
                        }
                        is NetworkResult.NoInternet -> {
                            _connectionState.emit(false)
                        }
                        is NetworkResult.Timeout -> {}
                    }
                    flag = true
                }
                .launchIn(viewModelScope)
    }
}