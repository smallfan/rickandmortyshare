package com.rm.ui.characters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.rm.databinding.CharactersItemBinding
import com.rm.domain.models.Character
import com.rm.util.setImageByUrl

class CharactersAdapter(
    var items: MutableList<Character> = mutableListOf(),
) : RecyclerView.Adapter<CharactersViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): CharactersViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return CharactersViewHolder(CharactersItemBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: CharactersViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun updateList(newItems: List<Character>) {
        val diffCallback = CharactersDiffCallback(items, newItems)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        diffResult.dispatchUpdatesTo(this)
        items = newItems.toMutableList()
    }
}

class CharactersViewHolder(
    private val binding: CharactersItemBinding,
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(item: Character) {
        binding.charPicImg.setImageByUrl(item.image)
        binding.charNameTxt.text = item.name
        binding.charSpeciesTxt.text = item.species
        binding.charStatusTxt.text = item.status
    }
}