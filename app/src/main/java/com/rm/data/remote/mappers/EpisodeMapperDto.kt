package com.rm.data.remote.mappers

import com.rm.data.db.models.EpisodeEntity
import com.rm.data.remote.NetworkResult
import com.rm.data.remote.data_sources.EpisodesResult
import com.rm.data.remote.models.EpisodeDto
import com.rm.domain.models.Episode
import com.rm.util.orZero
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

interface EpisodeMapperDto {
    fun mapToEpisode(dto: EpisodeDto): Episode
    fun mapToEpisodeEntity(dto: EpisodeDto, seasonNumber: Int): EpisodeEntity
    fun mapResult(list: List<Episode>): EpisodesResult
    fun mapResult(ex: Throwable): EpisodesResult
}

class EpisodeMapperImpl : EpisodeMapperDto {
    override fun mapToEpisode(dto: EpisodeDto): Episode {
        return Episode(
            id = dto.id.orZero(),
            name = dto.name.orEmpty(),
            air_date = dto.air_date.orEmpty(),
            episode = dto.episode.orEmpty(),
            characters = dto.characters.orEmpty(),
            url = dto.url.orEmpty(),
            created = dto.created.orEmpty()
        )
    }

    override fun mapToEpisodeEntity(dto: EpisodeDto, seasonNumber: Int): EpisodeEntity {
        return EpisodeEntity(
            id = dto.id.orZero(),
            seasonNumber = seasonNumber,
            name = dto.name.orEmpty(),
            air_date = dto.air_date.orEmpty(),
            episode = dto.episode.orEmpty(),
            characters = dto.characters.orEmpty(),
            url = dto.url.orEmpty(),
            created = dto.created.orEmpty()
        )
    }

    override fun mapResult(list: List<Episode>): EpisodesResult {
        return NetworkResult.Success(list)
    }

    override fun mapResult(ex: Throwable): EpisodesResult {
        return when (ex) {
            is UnknownHostException -> NetworkResult.NoInternet()
            is TimeoutException -> NetworkResult.Timeout()
            else -> NetworkResult.Exception(Throwable("Unknown exception"))
        }
    }
}