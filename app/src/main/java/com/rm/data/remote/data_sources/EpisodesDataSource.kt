package com.rm.data.remote.data_sources

import com.rm.data.db.AppDatabase
import com.rm.data.remote.NetworkResult
import com.rm.data.remote.api.ApiService
import com.rm.data.remote.mappers.EpisodeMapperDto
import com.rm.data.remote.models.EpisodeDto
import com.rm.domain.models.Episode
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

typealias EpisodesResult = NetworkResult<List<Episode>, Throwable>

interface EpisodesDataSource {
    fun getEpisodesOfSeason(episodesNumber: String, seasonNumber: Int): Flow<EpisodesResult>
}

@Singleton
class EpisodesDataSourceImpl @Inject constructor(
    private val apiService: ApiService,
    private val appDatabase: AppDatabase,
    private val mapper: EpisodeMapperDto,
) : EpisodesDataSource {

    override fun getEpisodesOfSeason(
        episodesNumber: String,
        seasonNumber: Int,
    ): Flow<EpisodesResult> = flow {
        val episodesDto = apiService.getEpisodesOfSeason(episodesNumber)
        insertEpisodeToBD(episodesDto, seasonNumber)
        val episodes = episodesDto.map {
            mapper.mapToEpisode(it)
        }
        emit(mapper.mapResult(episodes))
    }.catch { exception ->
        emit(mapper.mapResult(exception))
    }

    private fun insertEpisodeToBD(episodesDto: List<EpisodeDto>, seasonNumber: Int) {
        val episodeEntity = episodesDto.map {
            mapper.mapToEpisodeEntity(
                it,
                seasonNumber
            )
        }
        appDatabase.episodeDao().insertEpisodes(episodeEntity)
    }
}