package com.rm.data.remote

sealed class NetworkResult<T, E> {
    class Success<T, E>(val data: T) : NetworkResult<T, E>()
    class Exception<T, E>(val e: E) : NetworkResult<T, E>()
    class NoInternet<T, E> : NetworkResult<T, E>()
    class Timeout<T, E> : NetworkResult<T, E>()
}