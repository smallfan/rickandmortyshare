package com.rm.data.remote.mappers

import com.rm.data.remote.NetworkResult
import com.rm.data.remote.data_sources.CharactersResult
import com.rm.data.remote.models.CharacterDto
import com.rm.data.remote.models.CharacterResponseDto
import com.rm.data.remote.models.InfoDto
import com.rm.domain.models.Character
import com.rm.domain.models.CharacterResponse
import com.rm.domain.models.Info
import com.rm.util.orZero
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

interface CharacterMapperDto {
    fun map(dto: CharacterResponseDto): CharacterResponse
    fun map(dto: CharacterDto): Character
    fun mapResult(characterResponse: CharacterResponse): CharactersResult
    fun mapResult(ex: Throwable): CharactersResult
}

class CharacterMapperDtoImpl : CharacterMapperDto {
    override fun map(dto: CharacterResponseDto): CharacterResponse {
        return CharacterResponse(
            info = map(dto.info),
            results = dto.results.map(::map)

        )
    }

    private fun map(dto: InfoDto): Info {
        return Info(
            count = dto.count.orZero(),
            pages = dto.pages.orZero(),
            next = dto.next.orEmpty(),
            prev = dto.prev.orEmpty(),
        )
    }

    override fun map(dto: CharacterDto): Character {
        return Character(
            dto.id.orZero(),
            dto.name.orEmpty(),
            dto.status.orEmpty(),
            dto.species.orEmpty(),
            dto.type.orEmpty(),
            dto.gender,
            dto.image.orEmpty(),
            dto.episode.orEmpty(),
            dto.url.orEmpty(),
            dto.created.orEmpty()
        )
    }

    override fun mapResult(characterResponse: CharacterResponse): CharactersResult {
        return NetworkResult.Success(characterResponse)
    }

    override fun mapResult(ex: Throwable): CharactersResult {
        return when (ex) {
            is UnknownHostException -> NetworkResult.NoInternet()
            is TimeoutException -> NetworkResult.Timeout()
            else -> NetworkResult.Exception(Throwable("Unknown exception"))
        }
    }
}