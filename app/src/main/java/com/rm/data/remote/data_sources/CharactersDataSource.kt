package com.rm.data.remote.data_sources

import com.rm.data.remote.NetworkResult
import com.rm.data.remote.api.ApiService
import com.rm.data.remote.mappers.CharacterMapperDto
import com.rm.domain.models.CharacterResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

typealias CharactersResult = NetworkResult<CharacterResponse, Throwable>

interface CharactersDataSource {
    fun getCharacters(page: Int) : Flow<CharactersResult>
}

@Singleton
class CharactersDataSourceImpl @Inject constructor(
    private val apiService: ApiService,
    private val mapper: CharacterMapperDto
) : CharactersDataSource{
    override fun getCharacters(page: Int): Flow<CharactersResult> = flow {
        val characterResponseDto = apiService.getCharacters(page)
        val characterResponse = mapper.map(characterResponseDto)
        emit(mapper.mapResult(characterResponse))
    }.catch { exception ->
        emit(mapper.mapResult(exception))
    }
}