package com.rm.data.remote.api

import com.rm.data.remote.models.CharacterResponseDto
import com.rm.data.remote.models.EpisodeDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("episode/{episodes}")
    suspend fun getEpisodesOfSeason(
        @Path("episodes") episodes: String,
    ): List<EpisodeDto>

    @GET("character/")
    suspend fun getCharacters(
        @Query("page") pageNumber: Int,
    ): CharacterResponseDto
}