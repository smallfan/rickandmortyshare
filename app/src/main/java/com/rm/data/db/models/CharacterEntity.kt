package com.rm.data.db.models

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CharacterEntity(
    @NonNull
    @PrimaryKey
    val id: Int?,
    val name: String?,
    val status: String?,
    val species: String?,
    val type: String?,
    val gender: String?,
    val image: String?,
    val episode: List<String>?,
    val url: String?,
    val created: String?,
    var page: Int?,
)

data class OriginEntity(
    val name: String?,
    val url: String?,
)

data class LocationEntity(
    val name: String?,
    val url: String?,
)