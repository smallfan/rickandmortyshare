package com.rm.data.db.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class EpisodeEntity(
    @PrimaryKey
    val id: Int?,
    val seasonNumber: Int?,
    val name: String?,
    val air_date: String?,
    val episode: String?,
    val characters: List<String>?,
    val url: String?,
    val created: String?,
)