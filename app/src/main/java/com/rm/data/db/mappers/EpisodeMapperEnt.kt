package com.rm.data.db.mappers

import com.rm.data.db.models.EpisodeEntity
import com.rm.domain.models.Episode
import com.rm.util.orZero

interface EpisodeMapperEnt {
    fun map(ent: EpisodeEntity): Episode
}

class EpisodeMapperEntImpl : EpisodeMapperEnt {
    override fun map(ent: EpisodeEntity): Episode {
        return Episode(
            id = ent.id.orZero(),
            name = ent.name.orEmpty(),
            air_date = ent.air_date.orEmpty(),
            episode = ent.episode.orEmpty(),
            characters = ent.characters.orEmpty(),
            url = ent.url.orEmpty(),
            created = ent.created.orEmpty()
        )
    }
}