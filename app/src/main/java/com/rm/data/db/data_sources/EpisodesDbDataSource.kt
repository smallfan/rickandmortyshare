package com.rm.data.db.data_sources

import com.rm.data.db.AppDatabase
import com.rm.data.db.mappers.EpisodeMapperEnt
import com.rm.data.remote.NetworkResult
import com.rm.data.remote.data_sources.EpisodesResult
import com.rm.domain.models.Episode
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

interface EpisodesDbDataSource {
    fun getEpisodesOfSeason(seasonNumber: Int) : Flow<EpisodesResult>
}

@Singleton
class EpisodesDbDataSourceImpl @Inject constructor(
    private val appDatabase: AppDatabase,
    private val mapper: EpisodeMapperEnt
) : EpisodesDbDataSource {

    override fun getEpisodesOfSeason(seasonNumber: Int): Flow<EpisodesResult> = flow {
        val episodesEnt = appDatabase.episodeDao().getEpisodesBySeason(seasonNumber)
        val episodes = episodesEnt.map {
            mapper.map(it)
        }
        emit(NetworkResult.Success<List<Episode>, Throwable>(episodes) as EpisodesResult)
    }
}