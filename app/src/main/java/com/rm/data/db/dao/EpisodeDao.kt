package com.rm.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rm.data.db.models.EpisodeEntity

@Dao
interface EpisodeDao {

    @Query("SELECT * FROM episodeEntity WHERE seasonNumber = :seasonNumber")
    fun getEpisodesBySeason(seasonNumber: Int): List<EpisodeEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEpisodes(episodes: List<EpisodeEntity>)

}