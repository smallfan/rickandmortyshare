package com.rm.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.rm.data.db.dao.EpisodeDao
import com.rm.data.db.models.EpisodeEntity

@Database(entities = [EpisodeEntity::class], version = 1, exportSchema = false)
@TypeConverters(StringListConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun episodeDao(): EpisodeDao
}