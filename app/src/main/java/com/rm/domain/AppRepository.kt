package com.rm.domain

import com.rm.data.db.data_sources.EpisodesDbDataSource
import com.rm.data.remote.NetworkResult
import com.rm.data.remote.data_sources.CharactersDataSource
import com.rm.data.remote.data_sources.CharactersResult
import com.rm.data.remote.data_sources.EpisodesDataSource
import com.rm.data.remote.data_sources.EpisodesResult
import com.rm.util.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapMerge
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

interface AppRepository {
    fun getEpisodesOfSeason(episodesNumber: String): Flow<EpisodesResult>
    fun getCharacters(page: Int): Flow<CharactersResult>
}

@Singleton
class AppRepositoryImpl @Inject constructor(
    private val episodesDataSource: EpisodesDataSource,
    private val charactersDataSource: CharactersDataSource,
    private val episodesDbDataSource: EpisodesDbDataSource,
) : AppRepository {

    override fun getEpisodesOfSeason(episodesNumber: String): Flow<EpisodesResult> {
        return episodesDbDataSource.getEpisodesOfSeason(getSeasonNumber(episodesNumber))
            .flatMapMerge { cachedListResult ->
                val cachedList = (cachedListResult as? NetworkResult.Success)?.data.orEmpty()
                if (cachedList.isEmpty()) {
                    val seasonNumber = getSeasonNumber(episodesNumber)
                    episodesDataSource.getEpisodesOfSeason(episodesNumber, seasonNumber)
                } else {
                    flow {
                        emit(cachedListResult)
                    }
                }
            }
    }

    override fun getCharacters(page: Int): Flow<CharactersResult> {
        return charactersDataSource.getCharacters(page)
    }

    private fun getSeasonNumber(episodesNumber: String): Int {
        return when (episodesNumber) {
            EPISODES_OF_FIRST_SEASON -> 1
            EPISODES_OF_SECOND_SEASON -> 2
            EPISODES_OF_THIRD_SEASON -> 3
            EPISODES_OF_FOURTH_SEASON -> 4
            EPISODES_OF_FIFTH_SEASON -> 5
            else -> 0
        }
    }
}