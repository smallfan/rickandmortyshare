package com.rm.domain.models

data class Season(
    val seasonNumber: Int,
    val seasonPoster: Int,
    val episodesOfSeason: String,
)