package com.rm.domain.models

class CharacterResponse(
    val info: Info? = null,
    val results: List<Character>? = null,
)

data class Info(
    val count: Int,
    val pages: Int,
    val next: String,
    val prev: String,
)